

export interface IimageOwner{
    nsid: string,
    username: string,
    realname: string,
    location: string,
    iconserver: string,
    iconfarm: number,
    path_alias: string
   
}