import { Iimage } from "./iimage";

export interface IPhotoResponse{
    photos: Iimage;
    stat: string;
}