export interface IimageUsage{
    candownload: number,
    canblog: number,
    canprint: number,
    canshare: number
}