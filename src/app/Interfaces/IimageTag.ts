export interface IimageTag{
    id: string,
                    author: string,
                    authorname: string,
                    raw: string,
                    _content: string,
                    machine_tag: number
}