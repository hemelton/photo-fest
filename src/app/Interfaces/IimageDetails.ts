
import { IimageOwner } from "./IimageOwner";
import { IimageTitle } from "./IimageTitle";
import { IimageDecription } from "./ImageDescription";
import { IimageVisibility } from "./IimageVisibility";
import { IimageDates } from "./IimageDates";
import { IimageEditability } from "./IimageEditability";
import { IimagePublicEditability } from "./IimagePublicEditability";
import { IimageUsage } from "./IimageUsage";
import { IimageComments } from "./IimageComments";
import { IimageNotes } from "./IimageNotes";
import { IimagePeople } from "./IimagePeople";
import { IimageTags } from "./IimageTags";
import { IimageUrls } from "./IimageUrls";
export interface IimageDetails{
    id: string,
    secret: string,
    server: string,
    farm: number,
    dateuploaded: string,
    isfavorite: number,
    license: string,
    safety_level: string,
    rotation: number,
    originalsecret: string,
    originalformat: string,
    owner: IimageOwner,
    title: IimageTitle
    description: IimageDecription,
    visibility: IimageVisibility,
    dates: IimageDates,
    views: string,
    editability: IimageEditability,
    publiceditability: IimagePublicEditability,
    usage: IimageUsage,
    comments: IimageComments,
    notes: IimageNotes,
    people: IimagePeople,
    tags: IimageTags,
    urls: IimageUrls,
    media: string









    
}