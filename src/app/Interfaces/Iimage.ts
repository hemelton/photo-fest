import { IPhoto } from "./iphoto";

export interface Iimage{
    page:number,
    pages: number,
    perpage: number,
    total: string, 
    photo:IPhoto[]
}