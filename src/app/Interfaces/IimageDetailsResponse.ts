import { IimageDetails } from "./IimageDetails";

export interface IimageDetailsResponse{
    photo: IimageDetails,
    stat: string
}