import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPhoto } from 'src/app/Interfaces/iphoto';
import { PhotoService } from 'src/app/services/photo.service';

@Component({
  selector: 'app-photo-listing',
  templateUrl: './photo-listing.component.html',
  styleUrls: ['./photo-listing.component.scss']
})
export class PhotoListingComponent implements OnInit {

  searchKey : string;
  photos : IPhoto[];
  constructor(private activeRoute: ActivatedRoute, private photoService: PhotoService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(param=>{
      this.searchKey =param.city;
      this.getPhotos();
    })
  }

  private getPhotos(){
    this.photoService.getPhotos(this.searchKey).subscribe(response => {
      this.photos = response.photos.photo;
    })
  }
}
