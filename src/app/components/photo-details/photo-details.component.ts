import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IimageDetails } from 'src/app/Interfaces/IimageDetails';
import { PhotoService } from 'src/app/services/photo.service';

@Component({
  selector: 'app-photo-details',
  templateUrl: './photo-details.component.html',
  styleUrls: ['./photo-details.component.scss']
})
export class PhotoDetailsComponent implements OnInit {

  searchKey : string;
  photoDetails : IimageDetails;
  constructor(private activeRoute: ActivatedRoute, private photoService: PhotoService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(param =>{
      this.searchKey = param.id
      this.getPhotoDetails();    
    })

  }

 private getPhotoDetails(){
   this.photoService.getPhotoDetails(this.searchKey).subscribe(response =>{
     this.photoDetails = response.photo;
   })
 }
}
