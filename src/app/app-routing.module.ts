import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LocationListingComponent } from './components/location-listing/location-listing.component';
import { PhotoDetailsComponent } from './components/photo-details/photo-details.component';
import { PhotoListingComponent } from './components/photo-listing/photo-listing.component';

const routes: Routes = [{
  path: 'home', component: HomeComponent,
  children: [
    { path: '', component: LocationListingComponent},
    { path: 'photos/:city', component: PhotoListingComponent },
    { path: 'details/:id', component: PhotoDetailsComponent }]
},
{ path: '**', redirectTo:'home'}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
