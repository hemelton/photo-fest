import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IPhotoResponse } from '../Interfaces/IphotoResponse';
import { IimageDetailsResponse } from '../Interfaces/IimageDetailsResponse';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  constructor(private httpClient: HttpClient) { }
  // ${environment.baseUrl}
  getPhotos(keyword: string){
    return this.httpClient.get<IPhotoResponse>(`https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=f8191aaca7d90197fdb2262e6231db30&user_id=&text=${keyword}&safe_search=&content_type=json&format=json&nojsoncallback=1`);
   
  }

  getPhotoDetails(keyword : string){
    return this.httpClient.get<IimageDetailsResponse>(`https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=f8191aaca7d90197fdb2262e6231db30&photo_id=${keyword}&secret=4406d8222a&format=json&nojsoncallback=1`);
  }

}
